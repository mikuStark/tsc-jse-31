package ru.tsc.karbainova.tm.command.data;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class DataBINLoadCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "data-load-bin";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Load binary data";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final FileInputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NonNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NonNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();

    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[]{Role.ADMIN};
    }
}
