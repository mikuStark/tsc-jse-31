package ru.tsc.karbainova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.util.UUID;

@Setter
@Getter
@NoArgsConstructor
public class User extends AbstractEntity {

    @NonNull
    private String login;
    @NonNull
    private String passwordHash;

    private String email;

    private String firstName;

    private String lastName;

    private String middleName;

    private Role role = Role.USER;

    private Boolean locked = false;

}
